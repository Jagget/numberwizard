﻿using UnityEngine;

public class LevelManger : MonoBehaviour
{
	public void LoadLevel (string levelName)
	{
		Debug.Log ("Level load requested for: " + levelName);
		Application.LoadLevel (levelName);
	}
	
	public void QuitRequest ()
	{
		Debug.Log ("Qiut requested");
		Application.Quit ();
	}
}
