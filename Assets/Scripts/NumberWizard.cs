﻿using UnityEngine;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour
{
	int max;
	int min;
	int guess = -1000;
	int maxGuessesAllowed = 10;
	public Text GuessText;

	void Start ()
	{
		StartGame ();
	}
	
	void StartGame ()
	{
		max = 1000;
		min = 1;
		NextGuess ();
	}
	
	void NextGuess ()
	{
		int next;
		int loops = 0;
		
		maxGuessesAllowed = maxGuessesAllowed - 1;
		
		if (maxGuessesAllowed <= 0) {
			Application.LoadLevel ("Win");
		}
		
		do {
			next = Random.Range (min, max + 1);
			loops++;
		} while(guess == next && loops <= 1000);

		guess = next;
		GuessText.text = guess + "?";
	}
	
	public void GuessHigher ()
	{
		min = guess;
		NextGuess ();
	}
	
	public void GuessLower ()
	{
		max = guess;
		NextGuess ();
	}
}
